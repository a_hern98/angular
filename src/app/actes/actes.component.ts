import { Component, OnInit, Input } from '@angular/core';
import { Concierto } from '../Models/concierto';
@Component({
  selector: 'app-actes',
  templateUrl: './actes.component.html',
  styleUrls: ['./actes.component.css']
})

export class ActesComponent implements OnInit {
  isLogged: boolean;
  conciertos: Array<Concierto>;
  isConciertos: boolean;
  acteSeleccionar: Concierto;
  grupoSeleccionar: Concierto;
  constructor() {
    this.conciertos = [];
  }

  ngOnInit() {
    // console.log(this.conciertos.length);
    if (this.conciertos.length == 0) {
      this.isConciertos == false;
    } else {
      this.isConciertos == true;
    }

  }
  ngDoCheck() {
    this.conciertos = JSON.parse(localStorage.getItem('actes'));

  }

  enviarActe(concierto) {
    this.acteSeleccionar = concierto;
  }
  enviarGrupo(grupo) {
    this.grupoSeleccionar = grupo;
  }


}
