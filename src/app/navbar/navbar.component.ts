import { Component, OnInit } from '@angular/core';
import { Concierto } from '../Models/concierto';
@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  private isLogged: boolean;
  private isActes: boolean;
  constructor() {
   }

  ngOnInit() {
    
  }
  ngDoCheck(){
    if (localStorage.getItem("login")) {
      this.isLogged = true;
    } else {
      this.isLogged = false;
    }
    if (localStorage.getItem("actes")){
      this.isActes=true;
    } else {
      this.isActes=false;
    }
  }
   callCrearActes(){
    Concierto.crearActes();
  }

}
