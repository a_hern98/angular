import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ActesComponent } from './actes/actes.component';
import { ActeComponent } from './acte/acte.component';
import { GrupComponent } from './grup/grup.component';
import { AddActeComponent } from './add-acte/add-acte.component';
const routes: Routes = [
  { path: '', redirectTo: '/inicio', pathMatch: 'full' },
  { path: 'inicio', component: ActesComponent },
  { path: 'acte/:id', component: ActeComponent },
  { path: 'add', component: AddActeComponent },
  { path: 'grup/:id', component: GrupComponent }
];
@NgModule({
  exports: [ RouterModule ],
  imports: [ RouterModule.forRoot(routes) ]
})
export class AppRouting {}
