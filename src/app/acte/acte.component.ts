import { Component, OnInit, Input } from '@angular/core';
import { Concierto } from '../Models/concierto';
import { NavbarComponent } from '../navbar/navbar.component';
import { ActesComponent } from '../actes/actes.component';
@Component({
  selector: 'app-acte',
  templateUrl: './acte.component.html',
  styleUrls: ['./acte.component.css']
})
export class ActeComponent implements OnInit {
  @Input() concierto: Concierto;
  isLogged: boolean;
  constructor() { }

  ngOnInit() {
  }
  ngDoCheck() {
    if (localStorage.getItem("login")) {
      this.isLogged = true;
    } else {
      this.isLogged = false;
    }
  }

}
