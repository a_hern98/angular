import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { LoginComponent } from './login/login.component';
import { LogoutComponent } from './logout/logout.component';
import { ActesComponent } from './actes/actes.component';
import { ActeComponent } from './acte/acte.component';
import { GrupComponent } from './grup/grup.component';
import { AddActeComponent } from './add-acte/add-acte.component';
import { AppRouting } from './app.routing'
@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    LoginComponent,
    LogoutComponent,
    ActesComponent,
    ActeComponent,
    GrupComponent,
    AddActeComponent
  ],
  imports: [
    FormsModule,
    BrowserModule,
    AppRouting,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
