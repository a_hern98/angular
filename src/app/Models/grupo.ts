export class Grupo{
  nombre: string;
  integrantes: number;
  constructor(nombre: string, integrantes: number){
    this.nombre = nombre;
    this.integrantes = integrantes;
  }
}
