import {Grupo} from "./grupo";
export class Concierto{
  numero: number;
  fecha_s: string;
  fecha_e: string;
  grupo: Grupo;
  constructor(numero: number, fecha_s: string, fecha_e: string, grupo: Grupo){
    this.numero = numero;
    this.fecha_s = fecha_s;
    this.fecha_e = fecha_e;
    this.grupo = grupo;
  }
  public static crearActes(){
     let lds = new Grupo ('Lagrimas De Sangre', 7);
     let raiz = new Grupo ('La Raiz', 11);
     let c1 = new Concierto (1245, '2019/11/25 23:00:00', '2019/11/26 02:00:00', lds);
     let c2 = new Concierto (345, '2019/05/06 22:30:00', '2019/05/07 1:00:00', raiz);
     let conciertosList = [c1,c2];
     localStorage.setItem('actes', JSON.stringify(conciertosList));
     let grupos = [lds, raiz];
     localStorage.setItem('grupos', JSON.stringify(grupos));
   }
}
