import { Component, OnInit } from '@angular/core';
import { Grupo } from '../Models/grupo';
import { Concierto } from '../Models/concierto';
import { NavbarComponent } from '../navbar/navbar.component';
import { FormsModule } from '@angular/forms';
@Component({
  selector: 'app-add-acte',
  templateUrl: './add-acte.component.html',
  styleUrls: ['./add-acte.component.css']
})
export class AddActeComponent implements OnInit {
  grupos: Array<Grupo>;
  constructor() {
  this.grupos=[];
 }

  ngOnInit() {

  }
  ngDoCheck() {
    this.grupos= JSON.parse(localStorage.getItem('grupos'));
    console.log(this.grupos);
  }
  crearActe(grupo, date_s, date_e){
    let conciertos=JSON.parse(localStorage.getItem('actes'));
    let concierto= new Concierto(Math.floor(Math.random() * (6546 - 1)) + 1, date_s, date_e,grupo);
    conciertos.push(concierto);
    localStorage.setItem('actes', JSON.stringify(conciertos));
  }

}
